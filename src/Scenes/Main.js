import Enemies from "../GameObjects/Enemies";
import Phaser from 'phaser';

export default class Main extends Phaser.Scene{
    logo = null
    player = null
    enemies = []
    enemiesGroup = null
    cursors = null
    constructor(){
        super("Main")
    }
    hitEnemy(player, enemiesGroup) {
        this.scene.restart()
    }
    preload(){
        this.load.image("tiles", "assets/assets.png");
        this.load.image("background", "assets/water.png");
        this.load.image("slime", "assets/slime.png");
        this.load.spritesheet('player', 'assets/player.png', { frameWidth: 32, frameHeight: 64 });
        this.load.tilemapTiledJSON("map", "assets/map.json");
    }
    create(){
        // eslint-disable-next-line
        const background = this.add.image(600, 300, 'background');
        const map = this.make.tilemap({ key: "map" });
        const tileset = map.addTilesetImage("landscape", "tiles");

        const groundLayer = map.createStaticLayer("Ground", tileset, 0, 0);
        const grassLayer = map.createStaticLayer("Grass", tileset, 0, 0);
        const collidableLayer = map.createStaticLayer("Collidable", tileset, 0, 0);
        const aboveLayer = map.createStaticLayer("Above", tileset, 0, 0);

        // Create Player
        const spawnPoint = map.findObject(
            'Player',
            obj => obj.name === 'Spawn Point'
        )

        this.player = this.physics.add.sprite(spawnPoint.x, spawnPoint.y, "player");

        // Create Enemies
        this.enemies = map.createFromObjects('Enemies', 'Enemy', {})
        this.enemiesGroup = new Enemies(this.physics.world, this, [], this.enemies)

        // Collision settings
        groundLayer.setCollisionByProperty({ collides: true });
        grassLayer.setCollisionByProperty({ collides: true });
        collidableLayer.setCollisionByProperty({ collides: true });
        aboveLayer.setCollisionByProperty({ collides: true }).setDepth(10);

        // The player interacts with the world
        this.physics.add.collider(this.player, groundLayer);
        this.physics.add.collider(this.player, grassLayer);
        this.physics.add.collider(this.player, collidableLayer);
        this.physics.add.collider(this.player, aboveLayer);

        // The enemies interact with the world
        this.physics.add.collider(this.enemiesGroup, groundLayer);
        this.physics.add.collider(this.enemiesGroup, grassLayer);
        this.physics.add.collider(this.enemiesGroup, collidableLayer);
        this.physics.add.collider(this.enemiesGroup, aboveLayer);

        // The player can be hit by an enemy
        this.physics.add.collider(this.enemiesGroup, this.player, this.hitEnemy.bind(this), null)

        // Show collider shapes for collidable tiles in map
        const debug = this.add.graphics().setAlpha(0.75);
        collidableLayer.renderDebug(debug, {
            tileColor: null,
            collidingTileColor: new Phaser.Display.Color(180, 50, 250, 255),
            faceColor: new Phaser.Display.Color(40, 39, 37, 255)
        });

        // User input
        this.cursors = this.input.keyboard.createCursorKeys();

        // Animations
        const anims = this.anims;
        anims.create({
            key: "left",
            frames: anims.generateFrameNames("player", { start: 20, end: 29 }),
            frameRate: 10,
            repeat: -1
        });
        anims.create({
            key: "right",
            frames: anims.generateFrameNames("player", { start: 30, end: 39 }),
            frameRate: 10,
            repeat: -1
        });
        anims.create({
            key: "front",
            frames: anims.generateFrameNames("player", { start: 0, end: 7 }),
            frameRate: 10,
            repeat: -1
        });
        anims.create({
            key: "back",
            frames: anims.generateFrameNames("player", { start: 10, end: 17 }),
            frameRate: 10,
            repeat: -1
        });

        // Cameras
        const camera = this.cameras.main;
        camera.startFollow(this.player);
        camera.setBounds(0, 0, map.widthInPixels, map.heightInPixels);

    }
    update(){
        const prevVelocity = this.player.body.velocity.clone();

        this.player.body.setVelocity(0);

        if (this.cursors.left.isDown) {
            this.player.body.setVelocityX(-100);
            this.player.anims.play("left", true);
        } else if (this.cursors.right.isDown) {
            this.player.body.setVelocityX(100);
            this.player.anims.play("right", true);
        } else if (this.cursors.up.isDown) {
            this.player.body.setVelocityY(-100);
            this.player.anims.play("back", true);
        } else if (this.cursors.down.isDown) {
            this.player.body.setVelocityY(100);
            this.player.anims.play("front", true);
        } else {
            this.player.anims.stop();

            if (prevVelocity.x < 0) this.player.setTexture('player', 'left')
            else if (prevVelocity.x > 0) this.player.setTexture('player', 'left')
            else if (prevVelocity.y < 0) this.player.setTexture('player', 'back')
            else if (prevVelocity.y > 0) this.player.setTexture('player', 'front')

            // You can set any texture and the sprite will snap back to the front when idle
            // this.player.setTexture('player', 'left')
        }

    }
}